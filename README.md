# Gitlab - runner - terraform

## Topology

## Setup

### Gitlab runner - 141

#### Install Podman

```bash
sudo apt-get update
sudo apt-get -y install podman
```

#### Install Gitlab runner
```bash
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```

#### Change from docker to podman
```bash
# switch to the user
sudo su -l gitlab-runner

# prepare user environment so we can use user-scoped systemd
loginctl enable-linger $USER
echo 'export XDG_RUNTIME_DIR=/run/user/$(id -u)' >> .bash_profile
source .bash_profile

#start and enable `podman.socket`
systemctl --user enable --now podman.socket

# get path to the socket
systemctl --user status podman.socket | grep Listen
# example: /run/user/978/podman/podman.sock
```
config.toml:
```bash
[[runners]]
    [runners.docker]
        host = "unix:///run/user/978/podman/podman.sock"
```